# Copyright (C) 2017, Boris Ondercin <boris@algotech.io>
import sys
sys.path.append('../private_markets')
sys.path.append('../')

from market import Market, TradeException
import bitsomxn
import time
import base64
import hmac
import urllib.request
import urllib.parse
import urllib.error
import urllib.request
import urllib.error
import urllib.parse
import requests
import hashlib
#import sys
import json
import config


class PrivateBitsoBTCMXN(Market):
    market_url = "https://api.bitso.com"
    balance_relative_url = "/v3/balance/"
    buy_url = "https://www.bitstamp.net/api/buy/"
    sell_url = "https://www.bitstamp.net/api/sell/"

    def __init__(self):
        super().__init__()
        self.username = config.bitso_username
        self.password = config.bitso_password
        self.currency = "MXN"
        # self.get_info()

    def _send_request_old(self, url, params={}, extra_headers=None):
        headers = {
            'Content-type': 'application/json',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'User-Agent': 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
        }
        if extra_headers is not None:
            for k, v in extra_headers.items():
                headers[k] = v

        params['user'] = self.username
        params['password'] = self.password
        postdata = urllib.parse.urlencode(params).encode("utf-8")
        req = urllib.request.Request(url, postdata, headers=headers)
        response = urllib.request.urlopen(req)
        code = response.getcode()
        if code == 200:
            jsonstr = response.read().decode('utf-8')
            return json.loads(jsonstr)
        return None

    def _send_request(self, http_method, request_path):
        nonce = str(int(round(time.time() * 1000)))
        json_payload = ""

        # Create signature
        message = nonce + http_method + request_path + json_payload
        signature = hmac.new(self.password.encode('utf-8'),
                             message.encode('utf-8'),
                             hashlib.sha256).hexdigest()

        # Build the auth header
        auth_header = 'Bitso %s:%s:%s' % (self.username, nonce, signature)

        # Send request
        response = requests.get(self.market_url + request_path, headers={"Authorization": auth_header})

        if response.status_code == 200:
            jsonstr = response.content.decode('utf-8')
            return json.loads(jsonstr)

        return None


    def buy(self, amount, price):
        """Create a buy limit order"""
        params = {"amount": amount, "price": price}
        response = self._send_request(self.buy_url, params)
        if "error" in response:
            raise TradeException(response["error"])

    def sell(self, amount, price):
        """Create a sell limit order"""
        params = {"amount": amount, "price": price}
        response = self._send_request(self.sell_url, params)
        if "error" in response:
            raise TradeException(response["error"])

    def get_info(self):
        """Get balance"""
        response = self._send_request("GET", self.balance_relative_url)
        print (response)
        balance = {}
        if response:
            if response["success"] == True:
                for currency_balance in response["payload"]["balances"]:
                    balance[currency_balance["currency"].upper()] = currency_balance["available"]

        print(balance)



if __name__ == "__main__":
    privatebitsoBTCMXN = PrivateBitsoBTCMXN()
    privatebitsoBTCMXN.get_info()
    #print(fc.convert(12., "USD", "EUR"))
    #print(fc.convert(12., "EUR", "USD"))
    #print(fc.rates)
