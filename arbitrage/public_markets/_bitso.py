import urllib.request
import urllib.error
import urllib.parse
import json
from .market import Market

class Bitso(Market):
    def __init__(self, currency, symbol):
        super().__init__(currency)
        self.symbol = symbol
        self.update_rate = 30

    def update_depth(self):
        url = 'https://api.bitso.com/v3/order_book/?book=' + self.symbol
        req = urllib.request.Request(url, headers={
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "*/*",
            "User-Agent": "curl/7.24.0 (x86_64-apple-darwin12.0)"})
        res = urllib.request.urlopen(req)
        depth = json.loads(res.read().decode('utf8'))
        self.depth = self.format_depth(depth)

    def sort_and_format(self, l, reverse=False):
        l.sort(key=lambda x: float(x['price']), reverse=reverse)
        r = []
        for i in l:
            r.append({'price': float(i['price']), 'amount': float(i['amount'])})
        return r

    def format_depth(self, depth):
        bids = self.sort_and_format(depth['payload']['bids'], True)
        asks = self.sort_and_format(depth['payload']['asks'], False)
        return {'asks': asks, 'bids': bids}
