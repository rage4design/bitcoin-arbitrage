from ._bitso import Bitso

class BitsoMXN(Bitso):
    def __init__(self):
        super().__init__("MXN", "btc_mxn")
